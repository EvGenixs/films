package com.example.films_project;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DatabaseHelper_Films extends SQLiteOpenHelper {
    Context context;
    public DatabaseHelper_Films(@Nullable Context context){
        super(context,"films_db",null,1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String create = "CREATE TABLE FILMS (ID INTEGER, NAME TEXT PRIMARY KEY, RATING INTEGER)";
        sqLiteDatabase.execSQL(create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
    public boolean addOne(Film film){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("NAME",film.getName());
        cv.put("RATING",film.getRating());
        long res =sqLiteDatabase.insert("FILMS",null,cv);
        if (res == -1){
            return false;
        }else {
            return true;
        }
    }
    public void deleteOne(Film film){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String delete ="DELETE FROM FILMS WHERE NAME = " + film.getName();
        Cursor c = sqLiteDatabase.rawQuery(delete,null);
        if(c.moveToFirst()){
            Toast.makeText(context,"Deleted "+ film.getName(),Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(context,"Error with deleteOne",Toast.LENGTH_LONG).show();
        }
    }
    public ArrayList<Film> getAll(Context context){
        ArrayList<Film> films = new ArrayList<>();
        String query = "SELECT*FROM FILMS";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor =sqLiteDatabase.rawQuery(query,null);
        if(cursor.moveToFirst()) {
            do {
                int curId = cursor.getInt(0);
                String curName = cursor.getString(1);
                int curRat = cursor.getInt(2);
                Film newFilm = new Film(curId,curName, curRat);
                films.add(newFilm);
            } while (cursor.moveToNext());
        }
        else {
            try {
                Toast.makeText(context, "Error with getAll - try", Toast.LENGTH_LONG).show();
            }catch (Exception e){
                Toast.makeText(context, "Error with getAll " + e, Toast.LENGTH_LONG).show();
            }
        }
        cursor.close();
        sqLiteDatabase.close();
        return films;
    }
}
