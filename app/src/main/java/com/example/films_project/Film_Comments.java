package com.example.films_project;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Film_Comments extends AppCompatActivity {
    TextView film;
    String film_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.film_act);
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        film_name = sharedPreferences.getString("Saved_film", "");
        film.setText(film_name);
    }
}
